################### GLOBALS

var false = 0;
var true = 1;

var radar_update_time = 2;
var launch_update_time = 0.3;

var missile_delay_time = 0;
var ciws_delay_time = 0;

var AIR = 0;
var MARINE = 1;
var SURFACE = 2;
var ORDNANCE = 3;

var ACTIVE_MISSILE = 0;
var NUM_MISSILES = 7;
var ROUNDS = 30;
var RELOAD_TIME = 600;

var opfor_switch = "enemies/opfor-switch";
setprop(opfor_switch,0);
var friend_switch = "enemies/friend-switch";
setprop(friend_switch,0);

################### MISSILE INFO

var missile_name = "KN-06";
var missile_brevity = "Grumble";
var missile_max_distance = 85; #max distance in nm
var missile_min_distance = 2; #minimum distance in nm
var lockon_time = 6; #time in seconds it takes to lock on to a target
var launch_in_progress = 0;

################### IFF

var target = {
	new: func (callsign) {
		var m = { parents: [target] };
		m.callsign = callsign;
		m.fired = false;
		return m;
	},
};

########################### not used anymore #######################
var targets = {
	"pinto": target.new("pinto"),
	"Leto": target.new("Leto"),
	"YV-187": target.new("YV-187"),
	"swamp": target.new("swamp"),
	"S": target.new("S"),
	"G-UNTER": target.new("G-UNTER"),
	"J-Mav16": target.new("J-Mav16"),
	"Raider1": target.new("Raider1"),
	"AF-MA13": target.new("AF-MA13"),
	"KOL24M": target.new("KOL24M"),
	"SNOWY1": target.new("SNOWY1"),
	"SHARK-1": target.new("SHARK-1"),
	"jake": target.new("jake"),
	"Evading-target": target.new("Evading-target"),# for testing with AI targets
	"Slow-evading-target": target.new("Slow-evading-target"),
	"Fast-evading-target": target.new("Fast-evading-target"),
	"Very-fast-evading-target": target.new("Very-fast-evading-target"),
};
###################################################################

var targetsV2 = nil;# OO vector (to make removing item easier)

var buildTargetList = func {
	# rather crappy code, but is not ran so often, so dont really matter.
	if (getprop(opfor_switch) == 0 and getprop(friend_switch) == 0) {
		for(var i = 1; i<=12;i+=1) {
			# add new callsign to list
			var sign = getprop("enemies/e"~i);
			var tgt = lookup(sign);
			if (tgt == nil) {
				targetsV2.append(target.new(sign));
				print("added callsign: "~sign);
			}
		}
	}
	var removeList = [];
	foreach(tgt ; targetsV2.vector) {
		# schedule removed callsigns to be removed
		if (getprop(opfor_switch) == 0 and getprop(friend_switch) == 0) {
			if (tgt.callsign != getprop("enemies/e1") and tgt.callsign != getprop("enemies/e2") and tgt.callsign != getprop("enemies/e3") and tgt.callsign != getprop("enemies/e4") and tgt.callsign != getprop("enemies/e5") and tgt.callsign != getprop("enemies/e6") and tgt.callsign != getprop("enemies/e7") and tgt.callsign != getprop("enemies/e8") and tgt.callsign != getprop("enemies/e9") and tgt.callsign != getprop("enemies/e10") and tgt.callsign != getprop("enemies/e11") and tgt.callsign != getprop("enemies/e12")) {
				append(removeList, tgt);
				print("will remove callsign: "~tgt.callsign);
			}
		}
	}
	foreach(tgt ; removeList) {
		# remove callsigns
		call(func {targetsV2.remove(tgt);}, nil, var err = []);
	}
};

var lookup = func (sign) {
	
	foreach (tgt ; targetsV2.vector) {
		if (tgt.callsign == sign) {
			return tgt;
		}
	}
	if ( getprop(opfor_switch) == 1 and (left(sign, 5) != "OPFOR" and left(sign,5) != "opfor")) {
		print("creating new target in: " ~ sign);
		tgt = target.new(sign);
		targetsV2.append(tgt);
		return tgt;
	}
	if ( getprop(friend_switch) == 1 and ( left(sign, 5) == "OPFOR" or left(sign,5) == "opfor" )) {
		print("creating new target in: " ~ sign);
		tgt = target.new(sign);
		targetsV2.append(tgt);
		return tgt;
	}
	return nil;
}


################### MAIN LOOP

var scan = func() {

	buildTargetList();


	if ( getprop("/carrier/sunk") == 1 ) {
		return;
	}
	
	#### ITERATE THROUGH MP LIST ####
	var my_pos = geo.aircraft_position();
	foreach(var mp; props.globals.getNode("/ai/models").getChildren("multiplayer")){#change to "aircraft" or "tanker"  to shoot at scenario AI planes.
		#### DO WE HAVE FIRING SOLUTION... ####
		# is plane in range, do we still have missiles, and is a missile already inbound, and has it been 4 seconds since the last missile launch?
		var trigger = fire_control(mp, my_pos);
		#print("dist to target = " ~ dist_to_target);
		#### ... FOR THE MISSILE ####
		var lu = lookup(mp.getNode("callsign").getValue());
		if ( launch_in_progress == 0 and  trigger == true and lu != nil and lu.fired == false and ACTIVE_MISSILE <= NUM_MISSILES and ( systime() - missile_delay_time > 7 ) ) { #
			#print("callsign " ~ cs ~ " found at " ~ dist_to_target);
			missile_delay_time = systime();
			lu.fired = true;
			mp.getNode("unique",1).setValue(rand());
			armament.contact = radar_logic.Contact.new(mp, AIR);
			launch_in_progress = 1;
			missile_launch(mp, systime());			
		#### ... FOR THE CIWS ####
		} elsif ( trigger == 2 and ROUNDS > 0 and ( systime() - ciws_delay_time > 1.0 ) ) {
			armament.defeatSpamFilter("Gun Splash On : " ~ mp.getNode("callsign").getValue());
			print("CIWS fired | rounds remaining: " ~ ROUNDS ~ " | hit on: " ~ mp.getNode("callsign").getValue());
			ciws_delay_time = systime();
			ROUNDS = ROUNDS - 1;
		}
	}
	settimer(scan,radar_update_time);
}

################## FIRE CONTROL
################## ALL AI LOGIC RELATED TO FIGURING OUT IF A TARGET SHOULD BE SHOT AT SHOULD GO HERE.

var fire_control = func(mp, my_pos) {
	# gather some data about the target
	var ufo_pos = geo.Coord.new().set_latlon(mp.getNode("position/latitude-deg").getValue(),mp.getNode("position/longitude-deg").getValue(),(mp.getNode("position/altitude-ft").getValue() * 0.3048));
	var target_distance = my_pos.direct_distance_to(ufo_pos);
	var target_ground_distance = my_pos.distance_to(ufo_pos);
	var target_heading = mp.getNode("orientation/true-heading-deg").getValue();
	var relative_bearing = math.abs(geo.normdeg180(ufo_pos.course_to(my_pos) - target_heading));
	var target_altitude = mp.getNode("position/altitude-ft").getValue();
	var target_relative_altitude = target_altitude - props.globals.getNode("/position/altitude-ft").getValue();
	var target_radial_airspeed = (-1 * ( ( relative_bearing / 90 ) - 1 ) ) * mp.getNode("velocities/true-airspeed-kt").getValue();
	var target_relative_pitch = math.asin( target_relative_altitude / (target_distance * M2FT) ) * R2D;
	

	# can the radar see it?
	if ( mp.getNode("valid").getValue() == false ) { return false; }
	if ( radar_logic.isNotBehindTerrain(mp) == false ) { return false; }
	if ( math.abs(target_radial_airspeed) < 20 ) { return false; } # i.e. notching, landed aircraft
	if ( target_relative_pitch < 0.5 ) { return false; } # roughly 925 feet at 20 nm, 25 feet at half a nm.
	if ( target_distance * M2NM > missile_max_distance ) { return false; }
	
	# is this plane a friend or foe?
	var lu = lookup(mp.getNode("callsign").getValue());
	if ( lu == nil ) { return false; }
	
	# is the plane within the engagement envelope?
	# the numbers after target_radial_airspeed are ( offset_of_engagement_envelope / speed_to_apply_that_offset )
	# larger offset means it wont fire until the plane is closer.
	# for visualization: https://www.desmos.com/calculator/4gqdoyik4e
	
	if ( target_radial_airspeed < 0 ) {
		var vren = 1 + target_radial_airspeed * ( 0.15 / 750 );
	} else {
		var vren = 1 + target_radial_airspeed * ( -0.15 / 750 );
	}
	target_ground_distance = target_ground_distance * M2NM;
	var engagement_altitude = (vren * (-.0065 * math.pow(target_ground_distance,2)) + 0.25 * target_ground_distance + 20) * 6076.12;
	if ( target_altitude > engagement_altitude ) { return false; }
	
	# 
	var the_dice = rand();
	
	if ( the_dice > 0.20 ) {
		return true;
	} else {
		return false;
	}
}


################### MISSILE CONTROL

### missile reload

var reload = func() {
	#figure out how many to add
	for ( var i = 0; i <= NUM_MISSILES; i = i + 1 ) {
		if (armament.AIM.new(i,missile_name,missile_brevity) != -1) {
			#if statement just in case reload was called before all missiles were fired. Cause avoid calling search() on same missile twice.
			armament.AIM.active[i].status = 0;
			armament.AIM.active[i].search();
		}
	}
	ACTIVE_MISSILE = 0;
	print("Ship reloaded with "~(NUM_MISSILES+1)~" missiles.");
	setprop("/sim/messages/copilot", "SHIP reloaded with "~(NUM_MISSILES+1)~" missiles.");
}

### missile launch

var missile_launch = func(mp, launchtime) {
	if ( getprop("/carrier/sunk") == 1 ) {
		var lu = lookup(mp.getNode("callsign").getValue());
		if (lu != nil) {
			lu.fired = false;
		}
		launch_in_progress = 0;
		return;
	}
	if ( armament.AIM.active[ACTIVE_MISSILE].status == 1 and systime() - launchtime > lockon_time and radar_logic.isNotBehindTerrain(mp) == true ) {
		var brevity = armament.AIM.active[ACTIVE_MISSILE].brevity;
		armament.defeatSpamFilter(brevity ~ " at: " ~ mp.getNode("callsign").getValue());
		armament.AIM.active[ACTIVE_MISSILE].release();
		ACTIVE_MISSILE = ACTIVE_MISSILE + 1;
		print("Fired KN-06 #" ~ ACTIVE_MISSILE ~ " at: " ~ mp.getNode("callsign").getValue());
		setprop("/sim/messages/copilot", "Missile launch at "~mp.getNode("callsign").getValue());
		if (ACTIVE_MISSILE > NUM_MISSILES) {
			setprop("/sim/messages/copilot", "SHIP out of ammo");
			print("SHIP out of ammo.");
		}
		launch_in_progress = 0;
		return;
	} elsif ((systime() - launchtime) > (lockon_time*2) or radar_logic.isNotBehindTerrain(mp) == false) {
		# launch cancelled so it dont forever goes in this loop and dont allow for other firings.
		var lu = lookup(mp.getNode("callsign").getValue());
		if (lu != nil) {
			lu.fired = false;
		}
		setprop("/sim/messages/copilot", "SHIP Canceled launch at "~mp.getNode("callsign").getValue());
		launch_in_progress = 0;
		return;
	}
	settimer( func { missile_launch(mp, launchtime); },launch_update_time);
}

### missile set launched to false for target

var incoming_listener = func {
	var history = getprop("/sim/multiplay/chat-history");
	var hist_vector = split("\n", history);
	if (size(hist_vector) > 0) {
		var last = hist_vector[size(hist_vector)-1];
		var last_vector = split(":", last);
		var author = last_vector[0];
		var callsign = getprop("sim/multiplay/callsign");
		if (size(last_vector) > 1 and author == callsign) {
			var last1 = split(" ", last_vector[1]);
			if(size(last1) > 2 and last1[size(last1)-1] == "exploded"  or last1[size(last1)-1] == "disarmed") {
				print("missile hit");
				if (size(last_vector) > 3) {
					var type = last1[1];
					#callsign = target, type = missile
					last_vector[3] = right(last_vector[3],size(last_vector[3]) - 1);
					var lu = lookup(last_vector[3]);
					if ( lu != nil and type == missile_name ) {						
						lu.fired = false;
					}
				}
			}  elsif(size(last1) > 2 and last1[size(last1)-2] == "missed" ) {
				print("missile missed");
				var target = last1[size(last1)-1];
				var type = last1[1];
				#callsign = target, type = missile
				var lu = lookup(target);
				if (lu != nil) {
					lu.fired = false;
				}
			} 
		}
	}
}


################### MISC

var main_init_listener = setlistener("sim/signals/fdm-initialized", func {
	targetsV2 = std.Vector.new();# OO vector (to make removing item easier)
	reload();
	scan();
	setlistener("/sim/multiplay/chat-history", incoming_listener, 0, 0);
	aircraft.data.save(0.5);# save the target list every 30 seconds
	removelistener(main_init_listener);
	setlistener(opfor_switch, func {
		targetsV2 = std.Vector.new();# OO vector (to make removing item easier)
	 	}, 0, 0);
	setlistener(friend_switch, func {
		targetsV2 = std.Vector.new();# OO vector (to make removing item easier)
	 	}, 0, 0);
 }, 0, 0);