import subprocess
import configparser

# load up the info in the config parser
p = configparser.ConfigParser()
p.read('config.ini')
fg_dir = p.get('settings','fg_data')
fg_air = p.get('settings','fg_aircraft')
fg_sce = p.get('settings','fg_scenery')
fg_dow = p.get('settings','download_dir')
fg_mp  = p.get('settings', 'mp_server')
fg_mpo = p.get('settings', 'out_port')
delay_sec = p.get('settings', 'delay-sec')

assets = []
for section_name in p.sections():
    if section_name[0:5] == 'asset':
        assets.insert(int(section_name.split('_')[-1]), {
            'callsign': p.get(section_name, 'callsign'),
            'port': p.get(section_name, 'port'),
            'type': p.get(section_name,'type'),
            'lat': p.get(section_name, 'lat'),
            'lon': p.get(section_name, 'lon')
        })


if delay_sec > 0:
	import time
	time.sleep(delay_sec)

# for every asset defined (exists, name/lon/lat isn't empty) spawn an FGFS process

for asset in assets:
    if asset['callsign'] != "" and asset['port'] != "" and asset['type'] != "" and asset['lat'] != "" and asset['lon'] != "":
        subprocess.Popen([
                fg_dir + '\\bin\\fgfs.exe',
                '--fg-aircraft=' + fg_air,
                '--fg-scenery=' + fg_sce,
                '--download-dir=' + fg_dow,
                '--enable-terrasync',
                '--aircraft=' + asset['type'],
                '--callsign=' + asset['callsign'],
                '--multiplay=out,10,' + fg_mp + ',' + fg_mpo,
                '--multiplay=in,10,,' + asset['port'],
                '--lat=' + asset['lat'],
                '--lon=' + asset['lon'],
                '--console'
            ],
            creationflags=subprocess.CREATE_NEW_CONSOLE
        )
